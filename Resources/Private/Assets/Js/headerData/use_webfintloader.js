;
(function() {
    if(hive_thm_webfontloader_active) {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = ("https:" == document.location.protocol ? "https" : "http") + "://" + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + "/typo3conf/ext/hive_thm_webfontloader/Resources/Public/Assets/Js/webfontloader.min.js.gzip";
        a.type = "text/javascript";
        a.async = "true";
        b.parentNode.insertBefore(a, b)
    }
})();